#include "MyPort.h"
#include <iostream>
#include <qdebug.h>

//
Port::Port(QObject *parent) :
        QObject(parent) {
    Mes.RX_buf = Rxbuff.data();
    Mes.TX_buf = TxBuff;
    Mes.N_RX = 0;
    Mes.N_TX = 0;
    Mes.Request = 0;
    Mes.timeout = 0;
    connect(this, SIGNAL(sig_ReadInPort()), this, SLOT(ReadInPort()));
    connect(this, SIGNAL(sig_WriteToPort(void * , int)), this, SLOT(WriteToPort(void * , int)));
    connect(this, SIGNAL(sig_Write_Settings_Port(QString, int, int, int, int, int)), this,
            SLOT(Write_Settings_Port(QString, int, int, int, int, int)));
    connect(this, SIGNAL(sig_close_port()), this, SLOT(close_port()));
    connect(this, SIGNAL(sig_open_port()), this, SLOT(open_port()));
    connect(this, SIGNAL(sig_serial_free()), this, SLOT(serial_free()));
    //
    //   connect(this, SIGNAL(QSerialPort::errorOccurred( QSerialPort::SerialPortError )),this,SLOT(Event_port_Err(QSerialPort::SerialPortError )));
    // thisPort.errorOccurred(( QSerialPort::SerialPortError ) (thisPort.error()));

}

Port::~Port() {
    qDebug("RS port is destruct");
}

void Port::Write_Settings_Port(QString name, int baudrate, int DataBits, int Parity, int StopBits,
                               int FlowControl)//setup COM port
{
    Comname = name;
            foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts()) {


            //qDebug() << info.portName() << "\n";
        }
//if (X)thisPort.moveToThread(X);
    switch (baudrate) {
        case 0:
            baudrate = 4800;
            break;
        case 1:
            baudrate = 9600;
            break;
        case 2:
            baudrate = 19200;
            break;
        case 3:
            baudrate = 38400;
            break;
        case 4:
            baudrate = 57600;
            break;
        case 5:
            baudrate = 115200;
            break;
        default:
            baudrate = 9600;
            break;
    }


    set.name = name;
    set.baudRate = baudrate;
    set.parity = (QSerialPort::Parity) Parity;
    set.dataBits = (QSerialPort::DataBits) DataBits;
    set.stopBits = (QSerialPort::StopBits) StopBits;
    set.flowControl = (QSerialPort::FlowControl) FlowControl;
    qDebug() << "Setting ";

    thisPort.close();
    thisPort.setPortName(name);
    thisPort.setBaudRate(baudrate);
    thisPort.setDataBits(set.dataBits);
    thisPort.setParity(set.parity);
    thisPort.setStopBits(set.stopBits);
    thisPort.setFlowControl(set.flowControl);
    thisPort.open(QSerialPort::ReadWrite);
    thisPort.clear();

    bool x = thisPort.isOpen();

    if (x) {
        qDebug() << "Description: " << name << "\n";
        qDebug() << (name + " Open");


    } else {

        thisPort.close();

    }

}


void Port::WriteToPort(void *adr, int N)//, char dat4, char dat5, char dat6, char dat7)
{
    is_busy = 0;
    if ((QSerialPort::SerialPortError::NoError != thisPort.error())) {
        close_port();
        Write_Settings_Port(set.name, set.baudRate, set.dataBits, set.parity, set.stopBits,
                            set.flowControl);//setup COM port
    }

    Mes.N_TX = N;

    Mes.TX_buf = (char *) adr;
    if (!is_busy) {
        is_busy = 1;
        if (!thisPort.isOpen())thisPort.open(QSerialPort::ReadWrite);//если порт закрыт, его надо открыть)
        {
            int x = thisPort.write((char *) adr, N);
            if (x == N) {
                qDebug() << " TX ok" << Comname << "\n";

                if (Mes.timeout)Tmr->singleShot(Mes.timeout, this, SLOT(ReadInPort()));
            } else {
                qDebug() << " TX not ok" << Comname << "\n";
                is_busy = 0;
            }

        }
    }//qDebug()<<"busy";
}


void Port::ReadInPort() {

    static char wait = 0;
    if ((QSerialPort::SerialPortError::NoError != thisPort.error())) {
        close_port();
        Write_Settings_Port(set.name, set.baudRate, set.dataBits, set.parity, set.stopBits,
                            set.flowControl);//setup COM port
    }

    if (!thisPort.isOpen())
        thisPort.open(QSerialPort::ReadWrite);//если порт закрыт, его надо открыть

    unsigned int X = thisPort.bytesAvailable(); //есть ли байты  в буфере

    if ((X >= Mes.Request))// если есть - читаем
    {

        qDebug() << " RX ok" << Comname << "\n";
        Read(X);//Функция чтения
        wait = 0;

    } else {
        wait++;
        qDebug() << "RX" << Comname + " wait: " << QString::number(wait * 50) << " ms ";
        if (wait < 10) {
            Tmr0->singleShot(50, this, SLOT(ReadInPort()));

        } else {
            qDebug() << "max waiting " << Comname << "\n";
            Read(X);//Функция чтения
            wait = 0;

            is_busy = 0;
        }
    }
    //if (!wait)is_busy = 0;
}

void Port::close_port() {

    thisPort.clear();
    thisPort.close();
    //Toconsol("Port is closed");

}

void Port::open_port() {
    if (!thisPort.isOpen())thisPort.open(QSerialPort::ReadWrite);//если порт закрыт, его надо открыть)

}

void Port::serial_free() {

    thisPort.reset();
    Port::deleteLater();

}

void Port::Event_port_Err(QSerialPort::SerialPortError Err) {
    qDebug() << Err;
    thisPort.clearError();
    thisPort.clear();
    close_port();
}


void Port::Read(int X) {


    Rxbuff = thisPort.read(X);//считываем в буфер все что пришло
    Mes.N_RX = X;
    Mes.RX_buf = Rxbuff.data();
    thisPort.clear();
    qDebug() << " RX " << QString::number(X) << Comname << "\n";
    qDebug() << thisPort.error();
    thisPort.clearError();

    close_port();
    Write_Settings_Port(set.name, set.baudRate,set.dataBits, set.parity, set.stopBits, set.flowControl );
}

quint16 Port::crc16(char *data, uint8_t size) {
    static const uint16_t wCRCTable[] = {
            0X0000, 0XC0C1, 0XC181, 0X0140, 0XC301, 0X03C0, 0X0280, 0XC241,
            0XC601, 0X06C0, 0X0780, 0XC741, 0X0500, 0XC5C1, 0XC481, 0X0440,
            0XCC01, 0X0CC0, 0X0D80, 0XCD41, 0X0F00, 0XCFC1, 0XCE81, 0X0E40,
            0X0A00, 0XCAC1, 0XCB81, 0X0B40, 0XC901, 0X09C0, 0X0880, 0XC841,
            0XD801, 0X18C0, 0X1980, 0XD941, 0X1B00, 0XDBC1, 0XDA81, 0X1A40,
            0X1E00, 0XDEC1, 0XDF81, 0X1F40, 0XDD01, 0X1DC0, 0X1C80, 0XDC41,
            0X1400, 0XD4C1, 0XD581, 0X1540, 0XD701, 0X17C0, 0X1680, 0XD641,
            0XD201, 0X12C0, 0X1380, 0XD341, 0X1100, 0XD1C1, 0XD081, 0X1040,
            0XF001, 0X30C0, 0X3180, 0XF141, 0X3300, 0XF3C1, 0XF281, 0X3240,
            0X3600, 0XF6C1, 0XF781, 0X3740, 0XF501, 0X35C0, 0X3480, 0XF441,
            0X3C00, 0XFCC1, 0XFD81, 0X3D40, 0XFF01, 0X3FC0, 0X3E80, 0XFE41,
            0XFA01, 0X3AC0, 0X3B80, 0XFB41, 0X3900, 0XF9C1, 0XF881, 0X3840,
            0X2800, 0XE8C1, 0XE981, 0X2940, 0XEB01, 0X2BC0, 0X2A80, 0XEA41,
            0XEE01, 0X2EC0, 0X2F80, 0XEF41, 0X2D00, 0XEDC1, 0XEC81, 0X2C40,
            0XE401, 0X24C0, 0X2580, 0XE541, 0X2700, 0XE7C1, 0XE681, 0X2640,
            0X2200, 0XE2C1, 0XE381, 0X2340, 0XE101, 0X21C0, 0X2080, 0XE041,
            0XA001, 0X60C0, 0X6180, 0XA141, 0X6300, 0XA3C1, 0XA281, 0X6240,
            0X6600, 0XA6C1, 0XA781, 0X6740, 0XA501, 0X65C0, 0X6480, 0XA441,
            0X6C00, 0XACC1, 0XAD81, 0X6D40, 0XAF01, 0X6FC0, 0X6E80, 0XAE41,
            0XAA01, 0X6AC0, 0X6B80, 0XAB41, 0X6900, 0XA9C1, 0XA881, 0X6840,
            0X7800, 0XB8C1, 0XB981, 0X7940, 0XBB01, 0X7BC0, 0X7A80, 0XBA41,
            0XBE01, 0X7EC0, 0X7F80, 0XBF41, 0X7D00, 0XBDC1, 0XBC81, 0X7C40,
            0XB401, 0X74C0, 0X7580, 0XB541, 0X7700, 0XB7C1, 0XB681, 0X7640,
            0X7200, 0XB2C1, 0XB381, 0X7340, 0XB101, 0X71C0, 0X7080, 0XB041,
            0X5000, 0X90C1, 0X9181, 0X5140, 0X9301, 0X53C0, 0X5280, 0X9241,
            0X9601, 0X56C0, 0X5780, 0X9741, 0X5500, 0X95C1, 0X9481, 0X5440,
            0X9C01, 0X5CC0, 0X5D80, 0X9D41, 0X5F00, 0X9FC1, 0X9E81, 0X5E40,
            0X5A00, 0X9AC1, 0X9B81, 0X5B40, 0X9901, 0X59C0, 0X5880, 0X9841,
            0X8801, 0X48C0, 0X4980, 0X8941, 0X4B00, 0X8BC1, 0X8A81, 0X4A40,
            0X4E00, 0X8EC1, 0X8F81, 0X4F40, 0X8D01, 0X4DC0, 0X4C80, 0X8C41,
            0X4400, 0X84C1, 0X8581, 0X4540, 0X8701, 0X47C0, 0X4680, 0X8641,
            0X8201, 0X42C0, 0X4380, 0X8341, 0X4100, 0X81C1, 0X8081, 0X4040};

    uint8_t nTemp;
    uint16_t wCRCWord = 0xFFFF;

    while (size--) {
        nTemp = *data++ ^ wCRCWord;
        wCRCWord >>= 8;
        wCRCWord ^= wCRCTable[nTemp];
    }
    return wCRCWord;
}
