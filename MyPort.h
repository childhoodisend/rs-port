#ifndef MYPORT_H
#define MYPORT_H


#include <QObject>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QThread>
#include <QTimer>
#include <QTime>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>


struct l_Mes {
    char *RX_buf;//RX buff adr
    char *TX_buf;//TX buff adr
    unsigned int N_RX;// bytes to recive
    unsigned int N_TX;// bytes to write
    unsigned int Request;// expect to receive(0 for any number)
    unsigned int timeout;
};


struct Settings {
    QString name;
    qint32 baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
};


class Port : public QObject {
Q_OBJECT

public:
    l_Mes Mes;

    explicit Port(QObject *parent = 0);

    ~Port();

    QTimer *Tmr = new QTimer();
    QTimer *Tmr0 = new QTimer();

    QSerialPort thisPort;
    Settings set;

    void connects(void);

private:

    QByteArray Rxbuff;
    char TxBuff[500];
    QString s_time, Comname;


    bool is_busy = 0;


signals:


    void sig_ReadInPort();

    void sig_WriteToPort(void *adr, int N);//
    void sig_Write_Settings_Port(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);

    void sig_close_port();

    void sig_open_port();

    void sig_serial_free();


public :


    void Read(int X);

    quint16 crc16(char *data, uint8_t size);


private slots:

    void ReadInPort();

    void WriteToPort(void *adr, int N);//
    void Write_Settings_Port(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);

    void close_port();

    void open_port();

    void serial_free();

    void Event_port_Err(QSerialPort::SerialPortError Err);
};


#endif // MYPORT_H
