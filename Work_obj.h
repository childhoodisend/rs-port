
#include <QDebug>
#include <QTimer>
#include <QTime>

#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QXmlStreamAttribute>
#include "MyPort.h"
#include <iostream>

class Work_obj : public QObject {
Q_OBJECT

public:
    QTimer *Tmr = new QTimer();
    Port *Port1 = new Port();;//Создаем обьект классa
    explicit Work_obj(QObject *parent = 0);

    char TX_buff[0xffff];
    int N_Tx;
    long long Total_Bytes_Number = 0;

signals:

    void sig_ReadInPort();

    void sig_WriteToPort(void *adr, int N);//
    void sig_Write_Settings_Port(QString name, int baudrate, int DataBits, int Parity, int StopBits, int FlowControl);

    void sig_close_port();

    void sig_open_port();

    void sig_serial_free();

public slots:

    void StartWork(char *adr, int N, int t);

    void Tmt_WriteToPort();
};

