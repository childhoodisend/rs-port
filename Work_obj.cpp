#include "Work_obj.h"

Work_obj::Work_obj(QObject *parent) :
        QObject(parent) {

    connect(Tmr, SIGNAL(timeout()), this, SLOT(Tmt_WriteToPort()));
    connect(this, SIGNAL(sig_ReadInPort()), Port1, SLOT(ReadInPort()));
    connect(this, SIGNAL(sig_WriteToPort(void * , int)), Port1, SLOT(WriteToPort(void * , int)));
    connect(this, SIGNAL(sig_Write_Settings_Port(QString, int, int, int, int, int)), Port1,
            SLOT(Write_Settings_Port(QString, int, int, int, int, int)));
    connect(this, SIGNAL(sig_close_port()), Port1, SLOT(close_port()));
    connect(this, SIGNAL(sig_open_port()), Port1, SLOT(open_port()));
    connect(this, SIGNAL(sig_serial_free()), Port1, SLOT(serial_free()));


}


void Work_obj::StartWork(char *adr, int N, int t) {

    sig_Write_Settings_Port("ttyUSB0", 5, 8, 0, 1, 0);
    Port1->Mes.Request = 1000; // 0
    //memcpy(TX_buff,adr,N);
    for (int i = 0; i < N; i++)TX_buff[i] = adr[i];
    N_Tx = 50;
    Port1->Mes.Request = N_Tx;
    Port1->Mes.timeout = 200;
    Tmr->setInterval(t * 2);
    Tmr->start();
}

void Work_obj::Tmt_WriteToPort() {

    qDebug() << "TX " << Port1->Mes.N_TX << " Bytes\n";
    qDebug() << "RX " << Port1->Mes.N_RX << " Bytes\n";

    std::cout << "TX " << Port1->Mes.N_TX << " Bytes\n";
    std::cout << "\n";
    std::cout << "TX " << Port1->Mes.N_RX << " Bytes\n";
    std::cout << "\n";

    for (int i = 0; i < Port1->Mes.N_TX; i++) {
        if (Port1->Mes.TX_buf[i] != Port1->Mes.RX_buf[i])
            qDebug() << "error";
    }

    Total_Bytes_Number += Port1->Mes.N_TX;
    qDebug() << "Bytes exchanged********* " << QString::number(Total_Bytes_Number);
    for (int i = 0; i < Port1->Mes.N_TX; i++) Port1->Mes.TX_buf[i] = rand() & 0xff;
    sig_WriteToPort(TX_buff, N_Tx);

}
