#include <QCoreApplication>

#include "main.h"

using namespace std;

int main(int argc, char *argv[]) {
    QCoreApplication a(argc, argv);

    static char buf[0xff], j = 0;


    Work_obj work;//главный объект, в котором описнан работа


    for (int i = 0; i <= 9; i++) buf[i] = (j++) & 0xff;

    work.StartWork(buf, 250, 1000);

    return a.exec();

}
